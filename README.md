# CSSIFIER_RS

## Simple crate to convert XPath selectors to CSS selectors

based on [cssify](https://github.com/santiycr/cssify) but rewrited in Rust (RiiR FTW).

### dependencies:
- [Regex](https://crates.io/crates/regex)

### Usage
The usage is simple:
```
cssifier("//a/b")
// Some("a b")

cssifier("//a/b[@id='hello']")
// Some(a b#hello)

cssifier("//a/b[contains(text(), 'hello')]")
// Some(a b:contains(hello))
```

### Known issues
- [x] bug in 'contains' condition
- [ ] invalid XPath return empty String
- [ ] Compatible with WASM

Contributors are welcome :).
